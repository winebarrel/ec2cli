require 'cgi'
require 'base64'
require 'net/https'
require 'openssl'
require 'optparse'
require 'pp'
require 'rexml/document'

require 'ec2cli/ec2-error'
require 'ec2cli/ec2-endpoint'

module EC2
  class Client

    API_VERSION = '2013-02-01'
    SIGNATURE_VERSION = 2
    SIGNATURE_ALGORITHM = :SHA256

    attr_reader :endpoint
    attr_reader :region
    attr_accessor :debug

    def initialize(accessKeyId, secretAccessKey, endpoint_or_region)
      @accessKeyId = accessKeyId
      @secretAccessKey = secretAccessKey
      set_endpoint_and_region(endpoint_or_region)
      @debug = false
    end

    def set_endpoint_and_region(endpoint_or_region)
      @endpoint, @region = EC2::Endpoint.endpoint_and_region(endpoint_or_region)
    end

    def query(action, params = {})
      params = {
        :Action           => action,
        :Version          => API_VERSION,
        :Timestamp        => Time.now.getutc.strftime('%Y-%m-%dT%H:%M:%SZ'),
        :SignatureVersion => SIGNATURE_VERSION,
        :SignatureMethod  => "Hmac#{SIGNATURE_ALGORITHM}",
        :AWSAccessKeyId   => @accessKeyId,
      }.merge(params)

      signature = aws_sign(params)
      params[:Signature] = signature

     if @debug
        $stderr.puts(<<EOS)
---request begin---
#{params.pretty_inspect}
---request end---
EOS
      end

      https = Net::HTTP.new(@endpoint, 443)
      https.use_ssl = true
      https.verify_mode = OpenSSL::SSL::VERIFY_NONE

      source = nil
      res = nil

      https.start do |w|
        req = Net::HTTP::Post.new('/',
          'Host' => @endpoint,
          'Content-Type' => 'application/x-www-form-urlencoded'
        )

        req.set_form_data(params)
        res = w.request(req)

        source = res.body
      end

      if /<Response>\s*<Errors>\s*<Error>/ =~ source
        errors = []

        REXML::Document.new(source).each_element('//Errors/Error') do |element|
          code = element.text('Code')
          message = element.text('Message')
          errors << "#{code}:#{message}"
        end

        raise EC2::Error, errors.join(', ') unless errors.empty?
      end

      if @debug
        $stderr.puts(<<EOS)
---response begin---
#{source}
---response end---
EOS
      end

      unless res.kind_of?(Net::HTTPOK)
        raise EC2::Error, "#{res.code} #{res.message}"
      end

      return source
    end

    def aws_sign(params)
      params = params.sort_by {|a, b| a.to_s }.map {|k, v| "#{CGI.escape(k.to_s)}=#{CGI.escape(v.to_s)}" }.join('&')
      string_to_sign = "POST\n#{@endpoint}\n/\n#{params}"
      digest = OpenSSL::HMAC.digest(OpenSSL::Digest.const_get(SIGNATURE_ALGORITHM).new, @secretAccessKey, string_to_sign)
      Base64.encode64(digest).gsub("\n", '')
    end

  end # Client
end # EC2
