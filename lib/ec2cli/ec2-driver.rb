require 'forwardable'
require 'rexml/parsers/pullparser'

require 'ec2cli/ec2-client'
require 'ec2cli/ec2-parser.tab'

module EC2
  class Driver
    extend Forwardable

    class Rownum
      def initialize(rownum)
        @rownum = rownum
      end

      def to_i
        @rownum
      end
    end # Rownum

    def initialize(accessKeyId, secretAccessKey, endpoint_or_region)
      @client = EC2::Client.new(accessKeyId, secretAccessKey, endpoint_or_region)
    end

    def_delegators(
      :@client,
      :endpoint,
      :region,
      :set_endpoint_and_region,
      :debug, :'debug=')

    def execute(query, opts = {})
      parsed, script_type, script = EC2::Parser.parse(query)
      command = parsed.class.name.split('::').last.to_sym

      case command
      when :DESCRIBE_INSTANCES
        describe_instances(parsed.filter)
      when :DESCRIBE_IMAGES
        describe_images(parsed.filter, :all => parsed.all)
      when :RUN_INSTANCES
        run_instainces(parsed.image_id, parsed.options)
      when :START_INSTANCES
        start_instainces(parsed.instances)
      when :STOP_INSTANCES
        stop_instainces(parsed.instances)
      when :CREATE_TAGS
        create_tags(parsed.filter, parsed.tags)
      when :SHOW_REGIONS
        EC2::Endpoint.regions
      when :USE
        set_endpoint_and_region(parsed.endpoint_or_region)
        nil
      else
        raise 'must not happen'
      end
    end

    private

    def create_filter_params(filter)
      params = {}

      (filter || {}).each_with_index do |name_values, i|
        name, values, = name_values
        params["Filter.#{i+1}.Name"] = name

        values.each_with_index do |value, j|
          params["Filter.#{i+1}.Value.#{j+1}"] = value
        end
      end

      return params
    end

    def describe_instances(filter)
      params = create_filter_params(filter)
      source = @client.query('DescribeInstances', params)
      parser = REXML::Parsers::PullParser.new(source)

      rows = []
      instance_id = nil
      status = nil
      ip_addr = nil
      groups = nil
      instance_type = nil
      zone = nil
      tags = {}

      while parser.has_next?
        event = parser.pull
        next if event.event_type != :start_element

        case event[0]
        when 'instanceId'
          ip_addr = nil
          tags = {}
          instance_id = parser.pull[0]
        when 'instanceState'
          until event.event_type == :start_element and event[0] == 'name'
            event = parser.pull
          end

          status = parser.pull[0]
        when 'instanceType'
          instance_type = parser.pull[0]
        when 'availabilityZone'
          zone = parser.pull[0]
        when 'privateIpAddress'
          ip_addr = parser.pull[0] unless ip_addr
        when 'groupSet'
          groups = []

          until event.event_type == :end_element and event[0] == 'groupSet'
            event = parser.pull

            if event.event_type == :start_element and event[0] == 'groupName'
              groups << parser.pull[0]
            end
          end
        when 'tagSet'
          tag_key = nil

          until event.event_type == :end_element and event[0] == 'tagSet'
            event = parser.pull
            next unless event.event_type == :start_element

            case event[0]
            when 'key';   tag_key = parser.pull[0]
            when 'value'; tags[tag_key] = parser.pull[0]
            end
          end
        when 'ebsOptimized'
          row = [            tags['Name'],
            instance_id,
            status,
            ip_addr,
            instance_type,
            groups,
            tags,
            zone,
          ]

          yield row if block_given?
          rows << row
        end
      end

      return rows
    end # describe_instances

    def describe_images(filter, opts = {})
      filter ||= []
      params = {}
      owner = filter.find {|k, v| k =~ /\Aowner\Z/i }

      if owner
        filter.delete_if {|k, v| k == owner[0] }
        params['Owner.1'] = owner[1].first
      elsif not opts[:all]
        params['Owner.1'] = 'self'
      end

      unless filter.any? {|k, v| k == 'image-id' }
        filter << ['image-type', ['machine']]
      end

      params.update(create_filter_params(filter))
      source = @client.query('DescribeImages', params)
      parser = REXML::Parsers::PullParser.new(source)

      rows = []
      image_id = nil
      image_state = nil
      architecture = nil
      image_type = nil
      name = nil
      description = nil

      while parser.has_next?
        event = parser.pull
        next if event.event_type != :start_element

        case event[0]
        when 'imageId'
          image_id = parser.pull[0]
        when 'imageState'
          image_state = parser.pull[0]
        when 'architecture'
          architecture = parser.pull[0]
        when 'imageType'
          image_type = parser.pull[0]
        when 'name'
          name = parser.pull[0]
        when 'description'
          description = parser.pull[0]
        when 'hypervisor'
          row = [
            image_id,
            image_state,
            architecture,
            image_type,
            name,
            description,
          ]

          yield row if block_given?
          rows << row
        end
      end

      return rows
    end # describe_images

    def run_instainces(image_id)
      # XXX:
      params = {
        'ImageId'  => image_id,
        'MinCount' => 1,
        'MaxCount' => 1,
      }

      @client.query('RunInstances', params)
      Rownum.new(1)
    end

    def start_instainces(instances)
      instances = get_instaince_ids_by_names(instances)
      params = {}

      instances.each_with_index do |instance_id, i|
        params["InstanceId.#{i+1}"] = instance_id
      end

      @client.query('StartInstances', params)

      Rownum.new(instances.length)
    end

    def stop_instainces(instances)
      instances = get_instaince_ids_by_names(instances)
      params = {}

      instances.each_with_index do |instance_id, i|
        params["InstanceId.#{i+1}"] = instance_id
      end

      @client.query('StopInstances', params)

      Rownum.new(instances.length)
    end

    def create_tags(filter, tags)
      instance_ids = get_instance_ids(create_filter_params(filter))

      params = {}
      delete_params = {}
      i = j = 0

      instance_ids.each do |instance_id|
        tags.each do |key, value|
          if value
            params["ResourceId.#{i+1}"] = instance_id
            params["Tag.#{i+1}.Key"] = key
            params["Tag.#{i+1}.Value"] = value
            i += 1
          else
            delete_params["ResourceId.#{j+1}"] = instance_id
            delete_params["Tag.#{j+1}.Key"] = key
            j += 1
          end
        end
      end

      @client.query('CreateTags', params) unless params.empty?
      @client.query('DeleteTags', delete_params) unless delete_params.empty?

      Rownum.new(instance_ids.length)
    end

    def get_instaince_ids_by_names(id_or_names)
      id_or_names.map do |id_or_name|
        if id_or_name =~ /\Ai-[0-9a-z]+\Z/
          id_or_name
        else
          get_instance_id_by_name(id_or_name)
        end
      end
    end

    def get_instance_id_by_name(name)
      params = {
        'Filter.1.Name'  => 'tag:Name',
        'Filter.1.Value' => name
      }

      instance_ids = get_instance_ids(params)

      return instance_ids.first
    end


    def get_instance_ids(params)
      instance_ids = []

      source = @client.query('DescribeInstances', params)
      parser = REXML::Parsers::PullParser.new(source)

      while parser.has_next?
        event = parser.pull
        next if event.event_type != :start_element

        if event[0] == 'instanceId'
          instance_ids << parser.pull[0]
        end
      end

      return instance_ids
    end

  end # Driver
end # EC2
