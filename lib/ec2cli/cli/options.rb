require 'optparse'
require 'ostruct'

def parse_options
  options = OpenStruct.new
  options.access_key_id     = ENV['AWS_ACCESS_KEY_ID']
  options.secret_access_key = ENV['AWS_SECRET_ACCESS_KEY']
  options.ec2_endpoint_or_region = ENV['EC2_ENDPOINT'] || ENV['EC2_REGION'] || 'ec2.us-east-1.amazonaws.com'

  # default value
  options.debug       = false

  ARGV.options do |opt|
    opt.on('-k', '--access-key=ACCESS_KEY')          {|v| options.access_key_id          = v      }
    opt.on('-s', '--secret-key=SECRET_KEY')          {|v| options.secret_access_key      = v      }
    opt.on('-r', '--region=REGION_OR_ENDPOINT')      {|v| options.ddb_endpoint_or_region = v      }
    opt.on('',   '--debug')                          {    options.debug                  = true   }

    opt.on('-h', '--help') {
      puts opt.help
      puts
      print_help
      exit
    }

    opt.parse!

    unless options.access_key_id and options.secret_access_key and options.ec2_endpoint_or_region
      puts opt.help
      exit 1
    end
  end

  options
end
