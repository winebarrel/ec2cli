def print_help
  puts <<EOS
##### Query #####

DESC[RIBE] INSTANCES [WHERE name = '...' AND ...]
  Describes one or more of your instances.

DESC[RIBE] IMAGES [WHERE name = '...' AND ...]
DESC[RIBE] ALL IMAGES [WHERE [Owner = {amazon|aws-marketplace|self} AND] name = '...' AND ...]
  Describes the images.

RUN INSTANCES image_id
  Launches instances

START INSTANCES id_or_name
START INSTANCES (id_or_name, ...)
  Starts an Amazon EBS-backed AMI that you've previously stopped.

STOP INSTANCES id_or_name
STOP INSTANCES (id_or_name, ...)
  Stops an Amazon EBS-backed instance

SET TAGS name = value, ...  [WHERE name = '...' AND ...]
  Adds or overwrites one or more tags for the specified EC2 resource or resources.

USE region_or_endpoint
  changes an endpoint

SHOW REGIONS
  displays a region list

##### Command #####

.help                           displays this message
.quit | .exit                   exits sdbcli
.debug           (true|false)?  displays a debug status or changes it
.version                        displays a version

EOS
end
