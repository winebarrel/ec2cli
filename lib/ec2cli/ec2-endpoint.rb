require 'ec2cli/ec2-error'

module EC2
  class Endpoint

    ENDPOINTS = {
      'ec2.us-east-1.amazonaws.com'      => 'us-east-1',
      'ec2.us-west-2.amazonaws.com'      => 'us-west-2',
      'ec2.us-west-1.amazonaws.com'      => 'us-west-1',
      'ec2.eu-west-1.amazonaws.com'      => 'eu-west-1',
      'ec2.ap-southeast-1.amazonaws.com' => 'ap-southeast-1',
      'ec2.ap-southeast-2.amazonaws.com' => 'ap-southeast-2',
      'ec2.ap-northeast-1.amazonaws.com' => 'ap-northeast-1',
      'ec2.sa-east-1.amazonaws.com'      => 'sa-east-1',
    }

    def self.endpoint_and_region(endpoint_or_region)
      if ENDPOINTS.key?(endpoint_or_region)
        [endpoint_or_region, ENDPOINTS[endpoint_or_region]]
      elsif ENDPOINTS.value?(endpoint_or_region)
        [ENDPOINTS.key(endpoint_or_region), endpoint_or_region]
      else
        raise EC2::Error, "Unknown endpoint or region: #{endpoint_or_region}"
      end
    end

    def self.regions
      ENDPOINTS.values.dup
    end

  end # Endpoint
end # EC2
