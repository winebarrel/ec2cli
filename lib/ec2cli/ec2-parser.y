class Parser
options no_result_var
rule
  stmt : describe_stmt
       | run_stmt
       | start_stmt
       | stop_stmt
       | set_stmt
       | show_stmt
       | use_stmt

  describe_stmt : describe_or_desc INSTANCES where_clause
                  {
                    struct(:DESCRIBE_INSTANCES, :filter => val[2])
                  }
                | describe_or_desc IMAGES where_clause
                  {
                    struct(:DESCRIBE_IMAGES, :filter => val[2], :all => false)
                  }
                | describe_or_desc ALL IMAGES where_clause
                  {
                    struct(:DESCRIBE_IMAGES, :filter => val[3], :all => true)
                  }

  describe_or_desc : DESCRIBE | DESC

  where_clause :
               | WHERE expr_list
                 {
                   val[1]
                 }

  expr_list : expr
              {
                [val[0]]
              }
            | expr_list AND expr
              {
                val[0] + [val[2]]
              }

  expr : IDENTIFIER EQ value
         {
           [val[0], [val[2]]]
         }
       | IDENTIFIER EQ value_list
         {
           [val[0], val[2]]
         }

  run_stmt : RUN INSTANCES value run_option_clause
             {
               struct(:RUN_INSTANCES, :image_id => val[2], :options => val[3])
             }

  run_option_clause :
                    | run_option_list

  run_option_list : run_option
                    {
                      [val[0]]
                    }
                  | run_option_list ',' run_option
                    {
                      val[0] + [val[2]]
                    }

  start_stmt : START INSTANCES value
               {
                 struct(:START_INSTANCES, :instances => [val[2]])
               }
             | START INSTANCES value_list
               {
                 struct(:START_INSTANCES, :instances => val[2])
               }

  stop_stmt : STOP INSTANCES value
              {
                struct(:STOP_INSTANCES, :instances => [val[2]])
              }
            | STOP INSTANCES value_list
              {
                struct(:STOP_INSTANCES, :instances => val[2])
              }

  set_stmt : SET TAGS attr_list where_clause
                {
                  struct(:CREATE_TAGS, :tags => val[2], :filter => val[3])
                }

  attr_list : attr
              {
                [val[0]]
              }
            | attr_list ',' attr
              {
                val[0] + [val[2]]
              }

  attr : IDENTIFIER EQ value
         {
           [val[0], val[2]]
         }
        | IDENTIFIER EQ NULL
         {
           [val[0], nil]
         }

  show_stmt : SHOW REGIONS
              {
                struct(:SHOW_REGIONS)
              }

  use_stmt : USE IDENTIFIER
           {
             struct(:USE, :endpoint_or_region => val[1])
           }

  value : STRING | NUMBER

  value_list : '(' number_list ')'
               {
                 val[1]
               }
             | '(' string_list ')'
               {
                 val[1]
               }

  number_list : NUMBER
                {
                  [val[0]]
                }
              | number_list ',' NUMBER
                {
                   val[0] + [val[2]]
                }

  string_list : STRING
                {
                  [val[0]]
                }
              | string_list ',' STRING
                {
                   val[0] + [val[2]]
                }

---- header

require 'strscan'

module EC2

---- inner

KEYWORDS = %w(
  ALL
  AND
  DESCRIBE
  DESC
  IMAGES
  INSTANCES
  REGIONS
  RUN
  SET
  SHOW
  START
  STOP
  TAGS
  USE
  WHERE
)

KEYWORD_REGEXP = Regexp.compile("(?:#{KEYWORDS.join '|'})\\b", Regexp::IGNORECASE)

def initialize(obj)
  src = obj.is_a?(IO) ? obj.read : obj.to_s
  @ss = StringScanner.new(src)
end

@@structs = {}

def struct(name, attrs = {})
  unless (clazz = @@structs[name])
    clazz = attrs.empty? ? Struct.new(name.to_s) : Struct.new(name.to_s, *attrs.keys)
    @@structs[name] = clazz
  end

  obj = clazz.new

  attrs.each do |key, val|
    obj.send("#{key}=", val)
  end

  return obj
end
private :struct

def scan
  tok = nil

  until @ss.eos?
    if (tok = @ss.scan /\s+/)
      # nothing to do
    elsif (tok = @ss.scan /(?:=)/)
      sym = {
        '='  => :EQ,
      }.fetch(tok)
      yield [sym, tok]
    elsif (tok = @ss.scan KEYWORD_REGEXP)
      yield [tok.upcase.to_sym, tok]
    elsif (tok = @ss.scan /NULL/i)
      yield [:NULL, nil]
    elsif (tok = @ss.scan /`(?:[^`]|``)*`/)
      yield [:IDENTIFIER, tok.slice(1...-1).gsub(/``/, '`')]
    elsif (tok = @ss.scan /'(?:[^']|'')*'/) #'
      yield [:STRING, tok.slice(1...-1).gsub(/''/, "'")]
    elsif (tok = @ss.scan /"(?:[^"]|"")*"/) #"
      yield [:STRING, tok.slice(1...-1).gsub(/""/, '"')]
    elsif (tok = @ss.scan /\d+(?:\.\d+)?/)
      yield [:NUMBER, (tok =~ /\./ ? tok.to_f : tok.to_i)]
    elsif (tok = @ss.scan /[,\(\)\*]/)
      yield [tok, tok]
    elsif (tok = @ss.scan /[^\s]+/)
      yield [:IDENTIFIER, tok]
    else
      raise Racc::ParseError, ('parse error on value "%s"' % @ss.rest.inspect)
    end
  end

  yield [false, 'EOF']
end
private :scan

def parse
  yyparse self, :scan
end

def self.parse(obj)
  self.new(obj).parse
end

---- footer

end # EC2
