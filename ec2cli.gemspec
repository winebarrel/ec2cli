Gem::Specification.new do |spec|
  spec.name              = 'ec2cli'
  spec.version           = '0.1.1'
  spec.summary           = 'ec2cli is an interactive command-line client of Amazon EC2.'
  spec.require_paths     = %w(lib)
  spec.files             = %w(README) + Dir.glob('bin/**/*') + Dir.glob('lib/**/*')
  spec.author            = 'winebarrel'
  spec.email             = 'sgwr_dts@yahoo.co.jp'
  spec.homepage          = 'https://bitbucket.org/winebarrel/ec2cli'
  spec.bindir            = 'bin'
  spec.executables << 'ec2cli'
  spec.add_dependency('json')
end
